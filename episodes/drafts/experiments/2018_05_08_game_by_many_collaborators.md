Experiment
==========

**Rules**: each participant writes one panel and tries to be consistent with previous panels. Then he designs the next participant. Describe the main action in the present tense. (for example: *Pepper is singing*, not *Pepper was singing*)

---

@valvin

We are in Pepper’s bedroom during the middle of the night. Pepper is deeply sleeping while Carrot is just finding something in a chest. Carrot is trying to not make noise. In his paw, he’s holding a costume that looks like a super hero costume which can hide him enough not to be recognizable.

---

@xHire

Carrot is half on his way out of the chest (one leg down), carefully holding the costume in his mouth, paws on edge of the chest. His head and eyes are turned towards Pepper’s bed—making sure she still sleeps. He’s cautious not to have anyone notice him.

---

@imsesaok

The metal button on the costume falls out and makes a clanging noise. Carrot freaks out and jumps into the air. The chest door hits the wall and makes a loud bang.

---

@Midgard

Pepper turns around and groans. Carrot looks at her anxiously. Will she wake up?

---

@popolon

Carrot tries to pull the clothes out of the room, but a metal part scratches the ground.

---

@craigmaloney

Pepper mutters something incomprehensible. Carrot freezes in his tracks.

---

@Jookia

Pepper pulls the sheets up over herself to block out noise. The coast is clear.

---

@Nartance

Carrot sighs deeply. If Pepper would discover the truth...
But a little noise disturbs his thought. Like metal scratching the floor. Again?!
Carrot turns his head towards the noise. And what he sees surprises him a lot.

Indeed, a little creature tries to pull off the metal button of his costume, with great difficulty. It looks like a little woman, with short and messy hair held by a pink ribbon, wearing a dress made of leafs and flowers. But that’s not her first appearance which surprises Carrot, not even her small size. It’s the little wings that are flapping very quickly on her back, as hard was the effort to take the button.
The creature suddenly stops pulling the button. She looks at Carrot slowly, and seems to be terrorized.

“M... Mister Cat!” she stammers, terrified, with large gestures. “It... it’s not what you think! I... I just wanted to... I like shiny things... but... you can keep it! Please, Mister Cat! Don’t eat me!”

But Carrot’s feline instinct makes him swing his tail with great appetite. He prepares to jump on the prey...

---

@deevad

...but Carrot is interrupted at the last moment: a big shadow appears on the mirror of the bedroom of Pepper, a terrifying one with red eyes.

“Huuuu, Master, what are you doing here?” says the little fairy wearing a dress made of leafs.

---

@xHire

The dark contour of the mysterious figure raises its hand, points a finger at the clothes and slowly utters: “That’s… that’s the *Cloak of Eternal Chaosah Power.* My aeon-long search has finally come to an end. **YES!** Now, give it to me! It’s **mine!**”

Both Carrot and the fairy gaze at the red-eyed shadow with frozen poses.

---

@RJQuiralta

Meanwhile not too far from the bedroom, just outside the window, Carrot’s friends (the three cats, please insert names here) are waiting anxiously, each with a costume of their own, puzzled about what has taken Carrot so long...

---

@TheFaico

Suddenly, in front of Carrot's friends (names proposed: Asparagus, Zucchini and Radish; officially: Moustache, Tigrette and Big White) a boiling fog appears just in the middle of the darkness.
From it, a new creature appears. It looks like the terrorific one with red-eye, but this creature looks older, with blue-eye and doesn't cause fear at all.
He is Fig Orange aka Figor (intended pun of Igor, recurring servant's name), servant of the other one. He notice the cats, that looks at him astonished.
Figor doesn't say a word, he's going to try to enter to the witch's house, his master forgot something important.
